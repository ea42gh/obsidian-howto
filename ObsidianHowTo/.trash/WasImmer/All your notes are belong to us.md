Notes:
- [[I is a note]]

- [[Untitled Diagram.svg]]
-  [[Untitled Diagram 1.svg]]
# Tables
```python
import holoviews as hv; hv.extension('bokeh')
def show():
	return hv.Curve([1,3,2])
```

| Title | Stuff | Other |
|:------|:------|:------|
|     1 |     2 |     1 |
|     3 |     4 |     1 |  

|asfs  | other |  32 |
|:---|:---|:---|
| za | <text style="color:coral;">rqwrqwee</text> | 25423vdsf |
| rqwe | avfd | avfavfdd |  

#-----------------#
#- chart data    -#
#-----------------#
data.area:
  - time: 1246406400000
    temperature: [14.3, 27.7]
  - time: 1246492800000
    temperature: [14.5, 27.8]
  - time: 1246579200000
    temperature: [15.5, 29.6]
  - time: 1246665600000
    temperature: [16.7, 30.7]
  - time: 1246752000000
    temperature: [16.5, 25.0]
  - time: 1246838400000
    temperature: [17.8, 25.7]

data.line:
  - time: 1246406400000
    temperature: 21.5
  - time: 1246492800000
    temperature: 22.1
  - time: 1246579200000
    temperature: 23
  - time: 1246665600000
    temperature: 23.8
  - time: 1246752000000
    temperature: 21.4
  - time: 1246838400000
    temperature: 21.3

#-----------------#
#- chart options -#
#-----------------#
options:
  appendPadding: 8
  syncViewPadding: true
  tooltip:
    shared: true
    showMarkers: false
    showCrosshairs: true
    offsetY: -50

options.area:
  axes: {}
  meta:
    time:
      type: 'time'
      mask: 'MM-DD'
      nice: true
      tickInterval: 172800000
      range: [0, 1]
    temperature:
      nice: true
      sync: true
      alias: '温度范围'
  geometries:
    - type: 'area'
      xField: 'time'
      yField: 'temperature'
      mapping: {}

options.line:
  axes: false
  meta:
    time:
      type: 'time'
      mask: 'MM-DD'
      nice: true
      tickInterval: 172800000
      range: [0, 1]
    temperature:
      sync: 'temperature'
      alias: '温度'
  geometries:
    - type: 'line'
      xField: 'time'
      yField: 'temperature'
      mapping: {}
    - type: 'point'
      xField: 'time'
      yField: 'temperature'
      mapping:
        shape: 'circle'
        style:
          fillOpacity: 1
          
```
### plotly
use Ctrl-P to insert a template
#### Foo

```plotly
data:
  - x: [0,1,2,3,4]
    y: [0,1,0,2,-1]
```



- [ ] ```dataviewjs
//Some plotly examples require d3 library to work.
//Since it's large and used by few examples,
//I propose a workaround to import d3;
//You need to download dependency from https://d3js.org/d3.v7.min.js
//and place it in your vault.
let path = app.vault.adapter.basePath;//absolute path to your vault
var d3 = require(path+"/utils/d3.v7.min.js");

//Replace this block with any example from plotly.com
//NOTE: `Plotly.newPlot` won't work here, use `window.renderPlotly` instead
var data = [
{x:[0,1,2,3,4,5,6,7,8,9],y:[4,4,2,2,3,3,2,2,4,4]},
{x:[0,1,2,3,4,5,6,7,8,9],y:[3,3,1,1,2,2,1,1,3,3]}
];
var layout = {};
var config = {};

window.renderPlotly(this.container, data, layout, config)
```

# Calendar

![[versuchs!]]

# Columns
=== multi-column-start: ID_rkj8
```column-settings
Number of Columns: 2


Largest Column: standard
```

=== end-column ===
Another column....
=== multi-column-end

