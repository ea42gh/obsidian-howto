## Day Planner
```mermaid
gantt
    dateFormat  HH-mm
    axisFormat %H:%M
    %% Current Time: 11:00:22 PM
    section Tasks
    Setup for work     :09-30, 15mm
    Ciprian     :09-45, 120mm
    Gym     :11-45, 435mm
    Spanish Practice     :19-00, 60mm
    Kurt/Stan     :20-00, -120mm
    END     :18-00, 0mm
    section Breaks

```

### Morning Prep

- [x] 09:30 Setup for work
- [x] 09:45 Ciprian
- [x] 11:45 Gym
- [ ] Unstructured

### Evening
- [x] 19:00 Spanish Practice
- [x] 20:00 Kurt/Stan
- [ ] 18:00 END