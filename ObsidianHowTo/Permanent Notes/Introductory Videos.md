---
tags:  ["#index", "#HowTo"]
up: [[Index]]
---

# Introductory Videos
| Link                                                                                                                      | Brief Description                                                                                 |
| ------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------- |
| [FromSergio](https://www.youtube.com/playlist?list=PL7oLu8NfQd84_gsyqBVSVgUmCCgcvSZMx)                                    | mastering Obsidian Playlist                                                                       |
| [FromSergio: 5 Plugins](https://www.youtube.com/watch?v=Byy-QNgtHIg&ab_channel=FromSergio)                                | Calendar, Recent Files, Kalgan Boards                                                             |
| [Excalibrain: Plugins](https://www.youtube.com/watch?v=T4FkL7-gEew&ab_channel=Zsolt%27sVisualPersonalKnowledgeManagement) | Advanced Tables, Annotator, Dataview,<br> Hover Editor, Map View, BRAT<br>Tag Wrangler, Templater |
| [Chow: Intro](https://www.youtube.com/watch?v=a0a98Ko8e3g&ab_channel=DarylChow%2CPh.D.)                                                                                                             | Good Introduction                                                                                 |
