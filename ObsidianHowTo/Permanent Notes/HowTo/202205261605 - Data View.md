---
tags:  ["#HowTo/graph", "HowTo/scripts", "#javascript"]
up: [[Index]]
next: [[202206102007 - Templater]]
---

# DataView
scripting (may include javascript code!)

- [Github](https://blacksmithgu.github.io/obsidian-dataview)

**Example:**  create a table of all notes that are tagged with `#HowTo`
````
```dataview
table howto as HowTo
from #HowTo
```
````

Some experiments
- [[DataView_0]]
- [[DataView_1]]
- [[DataView_3]] ]
