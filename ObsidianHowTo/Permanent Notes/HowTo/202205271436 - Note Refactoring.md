---
tags:  ["#HowTo"]
up: [[Index]]
next: [[202205261605 - Data View]]
---

# Note Refactoring
split/merge Notes

- Install `Note Refactor` from Community Plugin
- [Github](https://github.com/lynchjames/note-refactor-obsidian)
- Use `<Ctrl P>/Node Refactor` commands