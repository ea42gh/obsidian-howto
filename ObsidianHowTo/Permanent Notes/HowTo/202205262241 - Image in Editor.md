---
tags:  ["#HowTo/setup"]
up: [[Index]]
next: [[202205261551 - Map View]]
---

# Ozan's Image in Editor
Show images in editor mode

- [Github](https://github.com/ozntel/oz-image-in-editor-obsidian)
- [Tutorial](https://www.youtube.com/watch?v=-a1vJVy20cQ&ab_channel=SantiYounger)
- Install `Ozan's Image in Editor` from Community Plugin
- Insert Images and PDF files

The following link
```
![100](https://gitlab.com/ea42gh/elementary-linear-algebra/-/raw/master/notebooks/Figs/cat.png)
```

renders as

![100](https://gitlab.com/ea42gh/elementary-linear-algebra/-/raw/master/notebooks/Figs/cat.png)

