---
tags:  ["#HowTo/graph"]
up: [[202205232014 - Sketches and Drawings]]
next: [[202205241832 - Ant Charts]]
---

# Mermaid Graphs
- built in tool to draw graphs.  [Live Editor](https://mermaid-js.github.io/mermaid-live-editor/) [Gitlab Document](https://mermaid-js.github.io/mermaid/#/)
- Example Code
```text
gantt
    dateFormat  HH-mm
    axisFormat %H:%M
    %% Current Time: 3:39:16 PM
    section Tasks
    Setup for work     :09-30, 15mm
    Ciprian     :09-45, 120mm
    Gym     :11-45, 120mm
    Vale     :19-00, 60mm
    Kurt/Stan     :20-00, -120mm
    END     :18-00, 0mm
    section Breaks
```

```mermaid
gantt
    dateFormat  HH-mm
    axisFormat %H:%M
    %% Current Time: 3:39:16 PM
    section Tasks
    Setup for work     :09-30, 15mm
    Ciprian     :09-45, 120mm
    Gym     :11-45, 120mm
    Vale     :19-00, 60mm
    Kurt/Stan     :20-00, -120mm
    END     :18-00, 0mm
    section Breaks
```
```mermaid
graph TD;  
	A-->B;  
	A-->C;  
	B-->D;  
	C-->D;  
```
