---
up: [[Index]]
tags: [ ]
next: [[]]
---

# QuickAdd
Automatically run a series of actions in succession (e.g., a script)

- install `QuickAdd` from Community Plugins
- [Minimal Guide](https://minimal.guide/Plugins/QuickAdd)
- [Movie Database Example](https://minimal.guide/Guides/Create+a+movie+database)

