---
tags:  ["#HowTo/graph"]
up: [[Index]]
---

# Sketches and Drawings

| Name / Description | Invocation | Link |
| ------------------ | ---------- | ---- |
| Latex Snippets     | dm,        | [[202205240844 - Latex Suite]]     |
| Mermaid charts     | ` ```mermaid ``` ` | [[202205242135 - Mermaid Graphs]]   |
| Ant Charts         | `<Ctrl P> Charts`  | [[202205241832 - Ant Charts]]       |
| Excalidraw         | ribbon command     | [[202205251350  - Excalidraw]]      |
| Diagrams draw.io   | right click        | [[202205250917 - draw.io Diagrams]] |
| Plotly             | `<Ctrl P> Plotly ` | [[202205251349 - Plotly]]           |
