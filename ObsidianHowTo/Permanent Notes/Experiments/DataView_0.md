---
alias: "document"
last-reviewed: 2021-08-17
type: 'learn'
thoughts:
   rating: 8
   reviewable: false
value: 3
up: [[202205261605 - Data View]]
next: [[DataView_1]]
---

# Markdown Page
Basic Field:: Value
**Bold Field**:: Nice!

- [ ] I am a task with [metadata::value]!
- [x] I am another task with completed::2020-09-15

We are on page `= this.file.name`.
