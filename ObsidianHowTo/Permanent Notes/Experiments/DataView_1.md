---
tags: [ 'HowTo/experiments', 'Experiments/dataview']  
type: 'learn'  
rating: 0
other: 'what the heck???'
up: [[202205261605 - Data View]]
next: [[DataView_3]]
---
# Simple Tests
### 1. create a table
---
[Get startedwith dataview](https://medium.com/os-techblog/how-to-get-started-with-obsidian-dataview-and-dataviewjs-5d6b5733d4a4)
````
```dataview  
table file.ctime as "Created Time"  
from "Fleeting Notes"  
sort file.ctime desc  
```
````

results in the following table:

```dataview  
table file.ctime as "Created Time"  
from "Fleeting Notes"  
sort file.ctime desc

```

### 2.  List all files in the vault
---
````
```dataview  
list  

```
````

Note: needs an empty line in edit mode!

```dataview
list

```
### 3. Yaml entry
---
```dataview  
table rating as "Rating", dateRated as "Date Rated"  
from "Permanent Notes"  
where type = "learn"  
sort rating desc

```
### 4. Javascript
---
```dataviewjs  
dv.header(2, 'Permanent Notes');  
dv.taskList(dv.pages('#HowTo').file.tasks  
.where(t => !t.completed));  
```
