---
type: 'training provider'  
tags: ['Experiments/training', 'Experiments/provider']  
website: 'https://www.pluralsight.com'  
title: 'Pluralsight:<br>    note created using Javascript!'  
up: [[202205261605 - Data View]]
---

````
```dataviewjs  
   let pg = dv.current(); // set pg to be the current page, for brevity  
    
   dv.header(1, pg.title); // print the page title from YAML as H1  
   dv.header(3, 'Website'); // print 'Website' as an H3  
   dv.paragraph(pg.website) // print website url from YAML in a paragraph  
      
   dv.header(2, 'Courses Taken:'); // print 'Courses Taken' as an H2  
   dv.list(dv.pages('#training').where(p => p.provider && p.provider.contains(pg.title)).file.link);  
```
````
```dataviewjs  
   let pg = dv.current(); // set pg to be the current page, for brevity  
    
   dv.header(1, pg.title); // print the page title from YAML as H1  
   dv.header(3, 'Website'); // print 'Website' as an H3  
   dv.paragraph(pg.website) // print website url from YAML in a paragraph  
      
   dv.header(2, 'Courses Taken:'); // print 'Courses Taken' as an H2  
   dv.list(dv.pages('#training').where(p => p.provider && p.provider.contains(pg.title)).file.link);  
```
