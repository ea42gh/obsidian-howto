---
tags:  ["#HowTo/setup", "#zettelkasten", "index"]
down: [[202205231624 - Initial Setup]]
---
links: [[Introductory Videos]]

# Main Index
## Common Tasks

| **Description**                    | **Link**                                        |
|:---------------------------------- |:----------------------------------------------- |
| Initial Setup $\qquad\qquad\qquad$ | [[202205231624 - Initial Setup]] $\qquad\qquad$ |
| Key Commands                       | [[202205241508 - Key commands]]                 |
| Vaults and Plugins                 | [[202205231718 - New vault]]                    |
| Markdown and HTML                  | [[202205231647 - Markdown_html]]                |
| Calendars                          | [[202205231729 - Calendar]]                     |
| Checklist                          | [[202205231845 - Checklists]]                   |
| Tables                             | [[202205232108 - Tables]]                       |
| Multiple Columns                   | [[202205241055 - Multi Columns]]                |
| Tabs and Panes                     | [[202205241528 - Tabs and Panes]]               |
| Recent Files                       | [[202205251956 - Recent Files]]                 |
| Annotate PDF files                 | [[202205261710 - Annotate Pdf]]                 |
| Image linking                      | [[202205262241 - Image in Editor]]              |
| Add notes to a map                 | [[202205261551 - Map View]]                     |
| Kanban Board                       | [[202205252144 - Kanban Boards]]                |
| Git Backup                         | [[202205240832 - Git Backup]]                   |
| Navigation Hierarchies             | [[202205281237 - Breadcrumbs]]                  |
| Refactoring commands               | [[202205271436 - Note Refactoring]]             |
| Button                             | [[202205271436 - Note Refactoring]]             |
| Scripting                          | [[202205261605 - Data View]]                    |
| Templater scripting                | [[202206102007 - Templater]]                    |                           |
| Quick Add Templates                | [[202206091308 - QuickAdd]]                     |
| Sketches (Summary)                 | [[202205232014 - Sketches and Drawings]]        |



## Math, Plots

| **Description** $\qquad\qquad\qquad$ | **Link** $\qquad\qquad\qquad$       |
|:------------------------------------ |:----------------------------------- |
| Latex Suite                          | [[202205240844 - Latex Suite]]      |
| Ant Charts                           | [[202205241832 - Ant Charts]]       |
| Mermaid Charts                       | [[202205242135 - Mermaid Graphs]]  |
| Draw.io Diagrams                     | [[202205250917 - draw.io Diagrams]] |
| Excalidraw                           | [[202205251350  - Excalidraw]]      |
| Plotly                               | [[202205251349 - Plotly]]           |
| Map View                             | [[202205261551 - Map View]]         |
